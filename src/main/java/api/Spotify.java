package api;

import java.util.Base64;
import java.util.Map;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;


import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import com.google.gson.Gson;




public class Spotify {


	public static String getToken(String authStr) {

      String ret = "";
      //String authString = "dedd2d98819c4c26887ef424ae64ed92:f9088320ee6f4f039b9cda6bae391a4d";

    try{
      URL url = new URL("https://accounts.spotify.com/api/token");
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      final String userPassword = authStr;
      final String encodeAuthorization = Base64.getEncoder().encodeToString(authStr.getBytes());
      con.setRequestProperty("Authorization", "Basic " + encodeAuthorization);
      con.setDoOutput(true);
      con.connect();
  //送信したいデータ
      String param = "grant_type=client_credentials";
            //リクエストボディに送信したいデータを書き込む
      OutputStream os = con.getOutputStream();
      BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
      writer.write(param);
      writer.flush();
      writer.close();
            //クローズ処理
      os.close();

      int statusCode = con.getResponseCode();

      InputStream stream = con.getInputStream();
      StringBuffer sb = new StringBuffer();
      String line = "";
      BufferedReader br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }
      try {
        stream.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      String responseData = sb.toString();

      Gson gson = new Gson();
      Map map = gson.fromJson(responseData, Map.class);
      ret = (String)map.get("access_token");

    } catch (IOException e) {
      e.printStackTrace();
    }
    return ret;
  }



   public static void main1() throws Exception {

        // Sending get request
        URL url = new URL("http://example-url");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestProperty("Authorization","Bearer "+" Actual bearer token issued by provider.");
        //e.g. bearer token= eyJhbGciOiXXXzUxMiJ9.eyJzdWIiOiPyc2hhcm1hQHBsdW1zbGljZS5jb206OjE6OjkwIiwiZXhwIjoxNTM3MzQyNTIxLCJpYXQiOjE1MzY3Mzc3MjF9.O33zP2l_0eDNfcqSQz29jUGJC-_THYsXllrmkFnk85dNRbAw66dyEKBP5dVcFUuNTA8zhA83kk3Y41_qZYx43T

        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");


        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;

        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
        // printing result from response
        System.out.println("Response:-" + response.toString());

    }


}





/*
public class HttpURLConnectionExample {


    public static void main(String[] args) throws Exception {

        // Sending get request
        URL url = new URL("http://example-url");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestProperty("Authorization","Bearer "+" Actual bearer token issued by provider.");
        //e.g. bearer token= eyJhbGciOiXXXzUxMiJ9.eyJzdWIiOiPyc2hhcm1hQHBsdW1zbGljZS5jb206OjE6OjkwIiwiZXhwIjoxNTM3MzQyNTIxLCJpYXQiOjE1MzY3Mzc3MjF9.O33zP2l_0eDNfcqSQz29jUGJC-_THYsXllrmkFnk85dNRbAw66dyEKBP5dVcFUuNTA8zhA83kk3Y41_qZYx43T

        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");


        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;

        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
        // printing result from response
        System.out.println("Response:-" + response.toString());

    }
}*/


/*package Web;

import static spark.Spark.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.net.MalformedURLException;




//import java.util.Scanner;
//import org.apache.commons.lang.RandomStringUtils;
//import java.security.NoSuchAlgorithmException;
//import java.security.SecureRandom;

public class App{
  public static void main(){
    App.getspotifytoken();
  }
 public static String getspotifytoken(){
        String name = "";
        try {
                URL myURL = new URL("https://accounts.spotify.com/api/token");
                //URL myURL = new URL(serviceURL);
                HttpURLConnection myURLConnection = (HttpURLConnection)myURL.openConnection();
                String userCredentials = "BQCHJAXkzCk0SEoh9FBq9RhlkL4EVD-YykJn5VE-yQJpPeZ1PA7HeHJGqF4mrXKTkcBIIOIoPwee4OC3z28";
                String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
                myURLConnection.setRequestProperty ("Authorization", basicAuth);
                myURLConnection.setRequestMethod("POST");
                myURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                myURLConnection.setRequestProperty("Content-Length", "" + postData.getBytes().length);
                myURLConnection.setRequestProperty("Content-Language", "en-US");
                myURLConnection.setUseCaches(false);
                myURLConnection.setDoInput(true);
                myURLConnection.setDoOutput(true);
                //HttpURLConnection http = (HttpURLConnection)url.openConnection();
                //http.setRequestMethod("GET");
                myURLConnection.connect();
                DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = dbfactory.newDocumentBuilder();
                Document doc = builder.parse(http.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;              
                while ((line = br.readLine()) != null) {
                  sb.append(line+"\n");
                }
                br.close();
                   


                JSONObject json = new JSONObject(sb.toString()); 
                // Convert text to object
                System.out.println(json.toString(4)); // Print it with specified indentation
                  return json.toString(4);

                /*ここから先は一般的な DOM の処理
                // ルート要素
                Element root = doc.getDocumentElement();
                // System.out.println(root.getNodeName());
                
                // <row> ～ </row>
                // getElementsByTagName が一番直感的で確実
                NodeList nl1 = root.getElementsByTagName("channel");
                // 一つ目のノード
                Node nd1 = nl1.item(0);
                // System.out.println(nd1.getNodeName());
                
                // <fld3>名称</fld3>
                // Node を Element にキャストして getElementsByTagName を使う
                NodeList nl2 = ((Element)nd1).getElementsByTagName("ldWeather:source");
                Node nd2 = nl2.item(0);
                NodeList nl3 = ((Element)nd2).getElementsByTagName("pref");
                // 最初の row ブロックの fld3 の 列挙
                for( int i = 0; i < nl3.getLength(); i++ ) {
                    name += "<li><a href=>" + printAttribute("title",nl3.item(i)) + "&raquo;</a><ul>";                    
                    for( int a = 0; a < ((Element)nl3.item(i)).getElementsByTagName("city").getLength(); a++) {
                        name += "<li><a href=http://localhost:4567/weather?n="+ printAttribute("id",((Element)nl3.item(i)).getElementsByTagName("city").item(a))  +">" + printAttribute("title",((Element)nl3.item(i)).getElementsByTagName("city").item(a)) + "</a></li>";
                    }
                    name += "</ul></li>";
                }*/
                // 接続解除
               /*http.disconnect();
            }
            catch (Exception e) {
            }
            return name;

            DefaultHttpClient httpclient = new DefaultHttpClient(); 
HttpPost post = new HttpPost(http());//res-api");
post.setHeader("Content-Type","application/json");
post.setHeader("Authorization", "Bearer " + finalToken);

JSONObject json = new JSONObject();
// json.put ...
// Send it as request body in the post request 

StringEntity params = new StringEntity(json.toString());
post.setEntity(params);

HttpResponse response = httpclient.execute(post);
httpclient.getConnectionManager().shutdown();
    }
}











/*public class App{
  public static void main(String[] args){
    weather();
    
  }

  public static void weather(){
     get("/weather", (req, res) -> {
            String data = "";
            String text = "";
            try {
                String url = "http://weather.livedoor.com/forecast/webservice/json/v1?city=" + req.queryParams("n");
                String charset = "UTF-8";
                List<String> contents = read(url, charset);
                for(String str : contents) {
                    data += str;
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
            JsonNode json = null; 
            JSONArray jsonArray = null;
            try{
                ObjectMapper mapper = new ObjectMapper();
                json = mapper.readTree(data);
                //System.out.println(json.get("pinpointLocations").asText());
                jsonArray = new JSONObject(data).getJSONArray("pinpointLocations");
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
            
            String text1 = json.get("description").get("text").asText();
            text = json.get("title") + "<p>" + text1.replace("\n","</p><p>") + "</p>";

            return text;
            }); 
  }

public static List<String> read(String url,String charset) throws Exception {
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            URLConnection conn = new URL(url).openConnection();
            is = conn.getInputStream();
            isr = new InputStreamReader(is,charset);
            br = new BufferedReader(isr);
 
            ArrayList<String> lineList = new ArrayList<String>();
            String line = null;
            while((line = br.readLine()) != null) {
                lineList.add(line);
            }
            return lineList;
        }finally {
            try {
                br.close();
            }catch(Exception e) {
            }
            try {
                isr.close();
            }catch(Exception e) {
            }
            try {
                is.close();
            }catch(Exception e) {
            }
        }
    }*/






/*public static void sosuu(){
Scanner scanner=new Scanner(System.in);

//ランダムな５桁の数字
    String target = RandomStringUtils.randomNumeric(5);
    

  //public class PrimeNumber {
  //public static void main(String[] args) {
  //Scanner sc = new Scanner(System.in);
    int number =NextInt();
                //sc.nextInt();

    if (number <2) {
      System.out.println(target + "は素数ではありません。");
      return;
    } 
    
    if (number%2 == 0) { // 偶数は先にリターン
      System.out.println(target + "は素数ではありません。");
      return;
    }

//1と自分自身でしか割り切れない数
    for (int i = 3; i <= Math.sqrt(number); i+=2) {
      if (number%i == 0) {
        System.out.println(target + "は素数ではありません。");
        return;
      }
    }

    System.out.println(target + "は素数です。");
  }*/

















/*
 * This Java source file was generated by the Gradle 'init' task.
 *
package Web;



import static spark.Spark.*;
/*
public class App {
    public static void main(String[] args) {
        get("/hello", (req, res) -> "Hello World");
    }


public static boolean checkName(String name){
    boolean ret;
    if(name="Takeshi"){
        ret=true;
    }else{
        ret=false;
    }
}
}
*

import java.util.Scanner;
class App{
    public static void main(String[] args){
  get("/hello", (req, res) -> "Hello World");
      Scanner scanner=new Scanner(System.in);
      
      System.out.print("名前：");
      String firstName=scanner.next();

 System.out.print("名字：");
String lastName=scanner.next();
String name=firstName+" "+lastName;


System.out.print("年齢：");
int age=scanner.nextInt();

System.out.println("名前は"+name+"です");
System.out.println("年齢は"+age+"歳です");

  if(age>=20){
    System.out.println("成年者です");
  }else{
    System.out.println("未成年者です");
  }
    }
}*/


