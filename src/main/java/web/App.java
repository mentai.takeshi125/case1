package web;

import api.Spotify;
//import api.JImageIconTest3;
/*import image.Image;
import image.GameWindow;
import image.DrawCanvas;*/

import java.util.Base64;
import java.util.Map;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;


import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import com.google.gson.Gson;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;


import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.*;
import java.awt.BorderLayout;
import java.net.MalformedURLException;

import org.json.JSONArray;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;

import static spark.Spark.*;

public class App{
    
   /*public static void main(String[] args){
      //id:secret
      String token = Spotify.getToken("dedd2d98819c4c26887ef424ae64ed92:f9088320ee6f4f039b9cda6bae391a4d");
      
      System.out.println(token);
   }*/


 public static void main(String[] args) throws Exception {

        // Sending get request
        URL url = new URL("https://api.spotify.com/v1/artists/1EowJ1WwkMzkCkRomFhui7");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

          String token = Spotify.getToken("dedd2d98819c4c26887ef424ae64ed92:f9088320ee6f4f039b9cda6bae391a4d");

        conn.setRequestProperty("Authorization","Bearer "+token);
        //e.g. bearer token= eyJhbGciOiXXXzUxMiJ9.eyJzdWIiOiPyc2hhcm1hQHBsdW1zbGljZS5jb206OjE6OjkwIiwiZXhwIjoxNTM3MzQyNTIxLCJpYXQiOjE1MzY3Mzc3MjF9.O33zP2l_0eDNfcqSQz29jUGJC-_THYsXllrmkFnk85dNRbAw66dyEKBP5dVcFUuNTA8zhA83kk3Y41_qZYx43T

        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");


        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;

        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
        // printing result from response
        //System.out.println("Response:-" + response.toString());
   
   
         String data = response.toString();
	      JsonNode json = null; 
            JSONArray jsonArray = null;
            try{
                ObjectMapper mapper = new ObjectMapper();
                json = mapper.readTree(data);
                //System.out.println(json.get("pinpointLocations").asText());
                jsonArray = new JSONObject(data).getJSONArray("images");
            }catch (Exception e) {
                System.out.println(e.getMessage());

            }
            String page = json.get("images").get(0).get("url").asText();
            System.out.println(page);

            get("/band", (req, res) -> {
            return "<img src="+page+">";
        });

    }
   
      
}




